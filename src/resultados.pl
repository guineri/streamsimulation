#!/usr/bin/perl

use strict;
use warnings;
use POSIX;

my ($fileName, $lf, $inner, $outer, $tweetPack) = @ARGV;

open(FIN, "<$fileName");


my @val;
my $numTasks = 0;
my $avgExecTime = 0;
my $avgResponseTime = 0;
my $avgTotalQueueTime = 0;
my $line;
my $totalExecTime;

while(!eof(FIN)) {

	$line = <FIN>;
	chop($line);
	@val = split(/\t/, $line);

	if($val[0] eq 'ExecTime:'){
		$totalExecTime = $val[1];
	}
	else{

		$numTasks++;

		$avgTotalQueueTime = $avgTotalQueueTime + $val[0];
		$avgExecTime = $avgExecTime + $val[1]; 
		$avgResponseTime = $avgResponseTime + $val[2];		
	}
}
close(FIN);

$avgTotalQueueTime = $avgTotalQueueTime/$numTasks;
$avgExecTime = $avgExecTime/$numTasks; 
$avgResponseTime = $avgResponseTime/$numTasks;

#print "avgTotalQueueTime= " . $avgTotalQueueTime . "\navgExecTime= " . $avgExecTime . "\navgResponseTime= " . $avgResponseTime . "\nNumTasks= " . $numTasks . "\n";	
print $lf . "\t" . $inner . "\t" . $outer . "\t" . $tweetPack . "\t" . $avgTotalQueueTime . "\t" . $avgResponseTime . "\t" . $avgExecTime . "\t" . $numTasks . "\t" . $totalExecTime . "\n";