#!/bin/bash

for lf in 0.2 0.4 0.6 0.8 0.9 1.0 1.1 1.2; do
	for inner in 1 2 4 8; do
		outer=$(( 8/$inner ));
		for taskTime in 2.0 3.0 4.0; do
			dir="results/$1"
			fileLog="log_${lf}_${inner}_${outer}_${taskTime}_${2}"
			fileResult="resultados_${1}_${taskTime}"
			mkdir -p $dir
			echo -e "Executando LF: $lf Inner: $inner Outer: $outer TaskTime: $taskTime Speedup: $2\n";
			./run.sh $lf $inner $outer $taskTime $2 >> $dir/$fileLog
			sed -i 's/\,/\./g' $dir/$fileLog
			perl resultados.pl $dir/$fileLog $lf $inner $outer $taskTime >> $dir/$fileResult
		done
	done
done

sed -i 's/\./\,/g' $dir/$fileResult