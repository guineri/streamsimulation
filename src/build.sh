mkdir classes

CPFILES=''

JFILES=''
for f in $(find ./java/org/parallelus/samples/streamsimulation/ -name "*.java"); do
   JFILES=$JFILES' '$f
done

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/guineri/StudioProjects/StreamSimulation/src/libs/

javac -cp .:$CPFILES -d ./classes/ $JFILES
jar cf ./streamsimulation.jar -C classes/ .
rm -r classes
