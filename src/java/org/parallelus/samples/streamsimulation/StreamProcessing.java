package org.parallelus.samples.streamsimulation;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class StreamProcessing {

	public static double getNextTextTime(double lambda) {
		
		double U = 1.0 - Math.random();
		double L = -Math.log(U);

		double nextTask = L/lambda;
		
		return nextTask;
	}

	public static void main(String[] args) {

		/*===== Parse Args ===================*/
		double lf = Double.parseDouble(args[0]);//1.2	; //0.2; 0.4; 0.6; 0.8; 1.0; 1.2;
		int numInnerThreads = Integer.parseInt(args[1]);
		int numOuterThreads = Integer.parseInt(args[2]);
		double taskTimeBase = (Double.parseDouble(args[3]));
		double spd = (Double.parseDouble(args[4]));

		/*====== Throughput max =======*/
		double T = 5.031280827; //tempo de execucao da tarefa sequencial
		double N = 8;			//threads maximas no pipe	
		double error = 0.005;		
		int numTasks = 50;
		double nextTaskTime = 0.0;

		double max_throughput_rate = N/T;
		double arrival_rate_lf = lf*max_throughput_rate;
		double taskTimeBaseMin = taskTimeBase - (taskTimeBase*error);	
		double taskTimeBaseMax = taskTimeBase + (taskTimeBase*error);
      
      	//System.out.println(spd);
      	
      	StreamSimulationNativeCalls streamSimulationNative;
      	streamSimulationNative = new StreamSimulationNativeCalls(numOuterThreads,numInnerThreads);

		for(int task = 0; task < numTasks; task++) {	

			Random r = new Random(); 
    		double taskTime = taskTimeBaseMin + (r.nextDouble() * (taskTimeBaseMax - taskTimeBaseMin));	
    		//System.out.println(taskTimeBaseMin + " " + taskTimeBase + " " + taskTimeBaseMax + " = " + taskTime);

			nextTaskTime = getNextTextTime(arrival_rate_lf);
			//System.out.println("Task:\t" + task + "\tsleepTime:\t" + nextTaskTime);
			try {
			    Thread.sleep((long)(nextTaskTime*1000.0));  //1000 milliseconds is one second.
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}

			streamSimulationNative.streamTask(taskTime,spd);
		}
      	
      	streamSimulationNative.waitFinish();
		streamSimulationNative.killRuntime();
		System.exit(0);
	}

}