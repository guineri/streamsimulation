package org.parallelus.samples.streamsimulation;

import java.util.ArrayList;
import java.util.List;

public interface StreamSimulationOperator {
    void streamTask(double taskTime, double spd);
    void waitFinish();
    void killRuntime();
}
