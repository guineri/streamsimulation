package org.parallelus.samples.streamsimulation;

import java.util.ArrayList;
import java.util.List;

public class StreamSimulationNativeCalls implements StreamSimulationOperator {
    private long dataPointer;

    private native long nativeInit(int numOuterThreads, int numInnerThreads);
    private native void nativeCleanUp(long dataPointer);
    private native void nativeStreamSimulation(long dataPointer, double taskTime, double spd);
    private native void nativeWaitFinish(long dataPointer);
    private native void nativeKill(long dataPointer);

    @Override

    public void streamTask(double taskTime, double spd) {
        nativeStreamSimulation(dataPointer,taskTime, spd);
    }


    public void waitFinish(){
        nativeWaitFinish(dataPointer);
    }

    public void killRuntime(){
        nativeKill(dataPointer);
    }

     public boolean inited() {
        return dataPointer != 0;
    }

    StreamSimulationNativeCalls(int numOuterThreads, int numInnerThreads) {
        dataPointer = nativeInit(numOuterThreads,numInnerThreads);
    }

    protected void finalize() throws Throwable {
        try {
            if(dataPointer != 0)
                nativeCleanUp(dataPointer);
        } catch(Throwable t) {
            throw t;
        } finally {
            super.finalize();
        }
    }

    static {
        System.loadLibrary("StreamSimulation");
    }
}