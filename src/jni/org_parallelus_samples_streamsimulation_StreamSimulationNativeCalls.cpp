#include <parallelus/ParallelUS.hpp>
#include <stddef.h>
#include "org_parallelus_samples_streamsimulation_StreamSimulationNativeCalls.h"
#include <fstream>
#include "toy_kernels.h"
#include <streambuf>

using namespace parallelus;

struct NativeData {
    std::shared_ptr<Runtime> runtime;
    std::shared_ptr<Program> program;   
    int _numInnerThreads;
};

JNIEXPORT jlong JNICALL Java_org_parallelus_samples_streamsimulation_StreamSimulationNativeCalls_nativeInit
        (JNIEnv *env, jobject self, jint numOuterThreads, jint numInnerThreads) {

    JavaVM *jvm;
    env->GetJavaVM(&jvm);
    if(!jvm) return (jlong) nullptr;

    auto dataPointer = new NativeData();
    dataPointer->_numInnerThreads = numInnerThreads;
    dataPointer->runtime = std::make_shared<Runtime>(jvm,false,numOuterThreads);
    dataPointer->program = std::make_shared<Program>(dataPointer->runtime, toyKernelsSource);

    return (jlong) dataPointer;
}

JNIEXPORT void JNICALL Java_org_parallelus_samples_streamsimulation_StreamSimulationNativeCalls_nativeCleanUp
        (JNIEnv *env, jobject self, jlong dataLong) {
    auto dataPointer = (NativeData *) dataLong;
    delete dataPointer;
}
JNIEXPORT void JNICALL Java_org_parallelus_samples_streamsimulation_StreamSimulationNativeCalls_nativeStreamSimulation
        (JNIEnv *env, jobject self, jlong dataLong, jdouble taskTime, jdouble spd) {
    
    auto dataPointer = (NativeData *) dataLong;
    int num_threads = dataPointer->_numInnerThreads;

    auto task = std::make_unique<Task>(dataPointer->program);
    task->addKernel("toy_kernel1");

    double speedup = 1;
    //if(num_threads!=1) speedup = (4.8*(double)num_threads)/8.0;
    //if(num_threads!=1) speedup = (7.8*(double)num_threads)/8.0;
    if(num_threads!=1) speedup = (spd*(double)num_threads)/8.0;

    task->setConfigFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type) {

            double execTime = taskTime/speedup;
            //printf("Speedup: %.8f -> %.8f\n", speedup, execTime);
            usleep(execTime*1e6);
            int device_type = 1;
        	kernelHash["toy_kernel1"]
                    ->setArg(0, device_type, type)
                    ->setWorkSize(1,1,1,type);
        //}
    });

    task->setFinishFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type){   

    });
    
    dataPointer->runtime->submitTask(std::move(task),2);

}


JNIEXPORT void JNICALL Java_org_parallelus_samples_streamsimulation_StreamSimulationNativeCalls_nativeWaitFinish
        (JNIEnv *env, jobject self, jlong dataLong) {
	auto dataPointer = (NativeData *) dataLong;
	dataPointer->runtime->finish();
}

JNIEXPORT void JNICALL Java_org_parallelus_samples_streamsimulation_StreamSimulationNativeCalls_nativeKill
        (JNIEnv *env, jobject self, jlong dataLong) {
	auto dataPointer = (NativeData *) dataLong;
	dataPointer->runtime->kill();
}
