#ifndef TOY_KERNELS_H
#define TOY_KERNELS_H

/// OpenCL kernels are defined in this file.

const char toyKernelsSource[] =
    "__kernel void toy_kernel0(int device_type) { }\n"
    "__kernel void toy_kernel1(int device_type) { }\n"
    "__kernel void toy_kernel2(int device_type) { }\n"
    "__kernel void toy_kernel3(int device_type) { }\n"
    "__kernel void toy_kernel4(int device_type) { }\n"
    "__kernel void toy_kernel5(int device_type) { }\n"
    "__kernel void toy_kernel6(int device_type) { }\n"
    "__kernel void toy_kernel7(int device_type) { }\n"
    "__kernel void toy_kernel8(int device_type) { }\n"
    "__kernel void toy_kernel9(int device_type) { }\n"
    "__kernel void toy_kernel10(int device_type) { }\n"
    "__kernel void toy_kernel11(int device_type) { }\n"
    "__kernel void toy_kernel12(int device_type) { }\n"
    "__kernel void toy_kernel13(int device_type) { }\n";

#endif
